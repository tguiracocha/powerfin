
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:widgetsflutter/model/Customer.dart';

class SecItem {
  SecItem(this.key, this.value);

  final String key;
  final String value;
}

class SecureStorage
{

  final _storage = FlutterSecureStorage();

  Future<Customer> readAll() async {
    String idcustom;
    String lastname;
    String name;
    String email;
    List<SecItem> _items = [];


    final all = await _storage.readAll();
    _items = all.keys
        .map((key) => SecItem(key, all[key]))
        .toList(growable: false);



    for(int i=0;i<_items.length;i++){
      if(_items[i].key=='idcustom')
        idcustom=_items[i].value;

      if(_items[i].key=='name')
        name=_items[i].value;

      if(_items[i].key=='lastname')
        lastname=_items[i].value;

      if(_items[i].key=='email')
        email=_items[i].value;
    }

    return Customer(
      id: idcustom,
      lastname: lastname,
      name: name,
      email: email
    );
  }

  Future<String> readOnly(String key) async {
    return await _storage.read(key: key);
  }

  void deleteAll() async {
    await _storage.deleteAll();
    readAll();
  }

  void deleteOnly(String key) async{
    await _storage.delete(key: key);
    readAll();
  }

  void addNewItem(final String key, final String value) async {
    await _storage.write(key: key, value: value);
    readAll();
  }


}

