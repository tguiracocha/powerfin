
class Item {

  final String id;
  final int idAttribute;
  final String name;
  final double price;
  final String image;
  final int stock;
  final int quantity;

  Item(
      this.id,
      this.idAttribute,
      this.name,
      this.price,
      this.image,
      this.stock,
      this.quantity);


  Item increaseQuantity() {
    if(this.stock>=(this.quantity+1)) {
      int total = this.quantity + 1;
      return this.copyWith(quantity: total);
    }
    else{
      return this;
    }
  }

  Item decreaseQuantity() {
    if (this.quantity <= 0) return this;
    int total=this.quantity-1;
    return this.copyWith(quantity: total);
  }

  Item copyWith({int quantity}) {
    return Item(
        this.id,
        this.idAttribute,
        this.name,
        this.price,
        this.image,
        this.stock,
        quantity);

  }

  double calculateTotal() => this.price * this.quantity;

  double pricewithIVA()=>(this.price+((this.price*12)/100));

  int calculateStock()=> this.stock-this.quantity;


  Item.fromJson(Map<String, dynamic> json)
      : id=json["id"],
        idAttribute=json['cache_default_attribute'],
        name=json["name"],
        price=json["price"],
        image=json["image"],
        stock= json["stock"],
        quantity=1;

}