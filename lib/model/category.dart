class Category
{

  final String id;
  final String title;
  final List<Category> children;

  Category(
      this.id,
      this.title,
      [this.children=const<Category>[]]
  );



  Category.fromJson(Map<String, dynamic> json)
      : id=json["id"],
        title=json["title"],
        children=[];
}