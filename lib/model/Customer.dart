
class Customer{
  String id;
  String email;
  String passwd;
  String lastname;
  String name;
  int idGuest;

  Customer({
    this.id,
    this.email,
    this.passwd,
    this.lastname,
    this.name,
    this.idGuest,
  });

  Customer copyWith({String passwd}){
    return Customer(
      id:this.id,
      email:this.email,
      passwd:passwd,
      lastname:this.lastname,
      name:this.name,
      idGuest:this.idGuest);
  }

  Customer.fromJson(Map<String, dynamic> json,String idGuest)
      :id=json["id"],
        name=json["name"],
        lastname=json["lastname"],
        email=json["email"],
        passwd=json["passwd"],
        idGuest=int.parse(idGuest);

}

