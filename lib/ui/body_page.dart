import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:widgetsflutter/model/item.dart';


class BodyProductPage extends StatefulWidget {

  @override
  _BodyProductPageState createState() => _BodyProductPageState();
}


class _BodyProductPageState extends State<BodyProductPage> {
  @override
  Widget build(BuildContext context) {
    List<Item> items =[
      Item("1",0 ,"Producto 1", 1, null, 10,1),
      Item("1",0 ,"Producto 2", 1, null, 10,1),
      Item("1",0 ,"Producto 3", 1, null, 10,1),
      Item("1",0 ,"Producto 4", 1, null, 10,1),
      Item("1",0 ,"Producto 5", 1, null, 10,1)
    ];

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5),
        child: new GridView.builder(
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10),
          itemCount: items.length,
          itemBuilder: (context,index){
            return _itemBuilder(context, items[index]);
          },
        ));
  }

  Widget _itemBuilder(context, Item product) {

    return new SafeArea(
        child: new Container(
          height: 400,
          child: Card(
            child: new Column(
              children: <Widget>[
                SizedBox(height: 5.0),
                _imageProduct(product.image),
                Divider(),
                _name(product.name),
                Divider(),
                _price(product.price.toStringAsFixed(2)),
                Divider(),
                _addButton(product),
              ],
            ),
          ),
        ));
  }

  Widget _name(String name) {
    return SizedBox(
      height: 12,
      child: Text(
        name,
        maxLines: 2,
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _price(String price) {
    return SizedBox(
      height: 12,
      child: Text('Precio:\$ ' + price.toString()),
    );
  }

  Widget _addButton(Item product) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: Colors.orangeAccent
      ),
     child: Text('Agregar'),
      onPressed: () async {
        product = product.copyWith(quantity: 1);
        if (product.stock >= (product.quantity + 1)) {

        } else {
          _onWidgetDidBuild(() {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text('Producto sin STOCK'),
                backgroundColor: Colors.red,
              ),
            );
          });
        }
      },
    );
  }

  Widget _imageProduct(var image) {
      return SizedBox(
          height: 30,
          width:30,
          child: Stack(children: <Widget>[
            Positioned.fill(child: Image.asset('assets/img/default.jpg'))
          ]));

  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }
}
