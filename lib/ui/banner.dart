import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class AdvertisingBanner extends StatelessWidget {


  AdvertisingBanner();

  @override
  Widget build(BuildContext context) {

    final List<String> listBanner=[
      "assets/img/default_banner.jpg",
      "assets/img/default_banner.jpg",
      "assets/img/default_banner.jpg"
    ];

    return SliverList(
      delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
        return Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 8.00,bottom: 8),
              child: CarouselSlider(
               options: CarouselOptions(
                 aspectRatio: 16/9,
                 viewportFraction: 0.8,
                 initialPage: 0,
                 enableInfiniteScroll: true,
                 reverse: false,
                 autoPlay: true,
                 autoPlayInterval: Duration(seconds: 5),
                 autoPlayAnimationDuration: Duration(milliseconds: 1500),
                 enlargeCenterPage: true,
                 scrollDirection: Axis.horizontal,
                 height: 100,
               ),
                items: listBanner.map((i) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.symmetric(horizontal: 5.0),
                        decoration: BoxDecoration(
                            color: Colors.amber
                        ),
                        child: new Image.asset(
                          i,
                          fit: BoxFit.cover,
                          width: 100.00,
                        ),
                      );
                    },
                  );
                }).toList(),
              ),

            )
          ],
        );
      }, childCount: 1),
    );
  }
}

