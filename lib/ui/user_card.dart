import 'package:flutter/material.dart';

class UserCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: UserAccountsDrawerHeader(
        accountName: Text("Usuario Invitado"),
        accountEmail: Text("usuario@email.com"),
        onDetailsPressed: () {},
        decoration: new BoxDecoration(
          color: Colors.orangeAccent,
        ),
        currentAccountPicture: CircleAvatar(
          child: Text("Usuario".toString().substring(0, 1).toString()),
          backgroundColor: Colors.white70,
        ),
      ),
    );
  }
}
