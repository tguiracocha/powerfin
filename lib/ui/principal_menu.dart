import 'package:flutter/material.dart';
import 'package:widgetsflutter/model/category.dart';

class PrincipalMenu extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    Category menu = Category("root", "Inicio", [
      Category('001', "Verduras"),
      Category('001', "Bebidas"),
      Category('001', "PAnaderia"),
    ]);


    return Padding(
      padding: EdgeInsets.all(1.0),
      child: ListView.separated(
        scrollDirection: Axis.vertical,
        itemCount: 1,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return _buildMenu(menu);
        },
        separatorBuilder: (context, index) {
          return Divider();
        },
      ),
    );
  }

  Widget _buildMenu(Category root) {
    if (root.children.isEmpty) {
      return ListTile(
          leading: const Icon(Icons.bubble_chart),
          title: Text(root.title),
          onTap: () {});
    }
    return ExpansionTile(
      key: PageStorageKey<Category>(root),
      title: Text(root.title),
      children: root.children.map(_buildMenu).toList(),
    );
  }
}
