import 'package:flutter/material.dart';
import 'package:widgetsflutter/ui/user_card.dart';

import 'body_page.dart';
import 'principal_menu.dart';
import 'banner.dart';

class MyHomePage extends StatefulWidget {
  final String title;

  const MyHomePage({Key key, this.title}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String get title=> widget.title;
  ScrollController _scrollController = new ScrollController();

  @override
  Widget build(BuildContext context) {

    const Color primaryColor= Color.fromRGBO(255, 130, 0, 1); //Colors.orangeAccent;

    return Container(
      child: Scaffold(
        appBar: AppBar(
            elevation: 0.0,
            backgroundColor: primaryColor,
            centerTitle: true,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Image.asset(
                  'assets/img/logo1.png',
                  fit: BoxFit.fitWidth,
                  width: 80,
                  height: 35,
                ),
              ],
            ),
            actions: <Widget>[
              _iconButtonCar(),
            ]),
        drawer: Drawer(
            child: ListView(
              children: <Widget>[
                new UserCard(),
                new PrincipalMenu(),
              ],
            )),
        body: Stack(
          children: <Widget>[
            InkWell(
              splashColor: Colors.transparent,
              focusColor: Colors.transparent,
              highlightColor: Colors.transparent,
              hoverColor: Colors.transparent,
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: new NestedScrollView(
                      controller: _scrollController,
                      headerSliverBuilder:
                          (BuildContext context, bool innerBoxIsScrolled) {
                        return <Widget>[
                          AdvertisingBanner()
                        ];
                      },
                      body: BodyProductPage(),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _iconButtonCar() {
      return Stack(
      children: <Widget>[
        IconButton(
          icon: Icon(Icons.shopping_cart),
          color: Colors.white,
          tooltip: 'Carrito',
          onPressed: () {

          },
        ),
        new Positioned(
            child: new Stack(
              children: <Widget>[
                new Icon(Icons.brightness_1,
                    size: 20.0, color: Colors.green),
                new Positioned(
                    top: 4.0,
                    right: 5.5,
                    child: new Center(
                      child: new Text(
                        "2",
                        style: new TextStyle(
                            color: Colors.white,
                            fontSize: 11.0,
                            fontWeight: FontWeight.w500),
                      ),
                    )),
              ],
            ))
      ],
    );
  }

}
